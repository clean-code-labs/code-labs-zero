﻿using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Client.Utilities
{
    public class LocalStorage : ILocalStorage
    {
        public LocalStorage()
        {
        }

        public async Task RemoveAsync(string key)
        {
            Preferences.Remove(key);
        }

        public async Task SaveStringAsync(string key, string value)
        {
            Preferences.Set(key, value);
        }

        public async Task<string> GetStringAsync(string key)
        {
            var str = Preferences.Get(key, string.Empty);
            return str;
        }

        public async Task SaveStringArrayAsync(string key, string[] values)
        {
            await SaveStringAsync(key, values == null ? "" : string.Join('\0', values));
        }

        public async Task<string[]> GetStringArrayAsync(string key)
        {
            var data = await GetStringAsync(key);
            if (!string.IsNullOrEmpty(data))
                return data.Split('\0');
            return null;
        }

        public async Task<bool> LoggedIn()
        {
            var data = await GetStringAsync("user");
            return string.IsNullOrEmpty(data);
        }
    }
}
